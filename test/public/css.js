var app = require('../../app/index.js');
var chai = require('chai');
var request = require('supertest');
var assert = require('assert');
var should = require('should');

describe('GET CSS /css/', ()=>{
  it('/css/bootstrap.min.css should respond successfully with a css file', (done)=>{
    request(app).
      get('/css/bootstrap.min.css').
      send().
      expect('Content-Type', new RegExp('css', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);

        return done();
    });
  });
  it('/css/keyValueFileWriter.css should respond successfully with a css file', (done)=>{
    request(app).
      get('/css/keyValueFileWriter.css').
      send().
      expect('Content-Type', new RegExp('css', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);

        return done();
    });
  });
});
