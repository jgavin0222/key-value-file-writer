const fs = require('fs');
const path = require('path');
const dotEnv = require('dotenv').config();
const file = process.env.FILE ||
              path.join(__dirname, '../files/file.txt');
const fileJson = process.env.FILE_JSON ||
              path.join(__dirname, '../files/file.json');

function initialize(initFile, initFileJson, port){
  var filePath = initFile.substr(0, initFile.lastIndexOf('/'));
  var jsonFilePath = initFileJson.substr(0, initFileJson.lastIndexOf('/'));

  var typePath = fs.existsSync(filePath);
  var typeJsonPath = fs.existsSync(jsonFilePath);

  if(!typePath){
    throw new Error('Path to File does not exist');
  }

  if(!typeJsonPath){
    throw new Error('Path to File JSON does not exist');
  }

  return true;
}

function read(){
  var data = '';
  if(fs.existsSync(file)){
    data = fs.readFileSync(file, 'utf8');
  }

  return data;
}

function readData(){
  var data = [];
  var dataObject = [];
  if(fs.existsSync(fileJson)){
    data = fs.readFileSync(fileJson, 'utf8');
    dataObject = JSON.parse(data);
  }

  return dataObject;
}

function generate(keys){
  var line = 0;
  var dataObject = [];
  var response = 'failure';
  if(fs.existsSync(fileJson)){
    object = readData();
    fs.writeFileSync(file, '');
    for(line=0; line<keys.length; line+=1){
      fs.appendFileSync(file, object[keys[line]]+'\n');
    }
    response = 'success';
  }

  return response;
}

function writeData(data){
  var present = readData() || {};

  if(typeof data.key !== 'undefined' && typeof data.value !== 'undefined'){
    present[data.key] = data.value;
    fs.writeFileSync(fileJson, JSON.stringify(present));
  }

  return 'success';
}

function removeData(data){
  var present = readData() || {};

  if(typeof data.key !== 'undefined'){
    Reflect.deleteProperty(present, data.key);
    fs.writeFileSync(fileJson, JSON.stringify(present));
  }

  return 'success';
}

module.exports = {
  generate,
  initialize,
  read,
  readData,
  removeData,
  writeData,
  fileJson,
  file
};
