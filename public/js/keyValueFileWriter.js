var hostname = window.location.host;
var keyValueNum = 0;
var hiddenKeyValueNum = 0;

$(document).ready(function(){
  refreshServerText();
  generateKeyValues();
  updateKeyValueHiddenAssign();
});

function deleteKeyValue(keyValueNum){
  var key = $("#key"+keyValueNum).val();
  var dataObject = {
    data: {
      key
    }
  }
  $.ajax({
    type: 'DELETE',
    url: 'http://'+hostname+'/keys',
    data: JSON.stringify(dataObject),
    contentType: 'application/json',
    dataType: 'json',
    success: function(response){
      if(response.status !== 'success'){
        alert("Error processing your request.")
      }
      $("#kv"+keyValueNum).remove();
      updateKeyValueHiddenAssign();
    }
  });
}

function updateKeyValue(num){
  var key = $("#key"+num).val();
  var value = $("#value"+num).val();
  var dataObject = {
    data: {
      key,
      value
    }
  };
  $.ajax({
    type: 'POST',
    url: 'http://'+hostname+'/keys',
    data: JSON.stringify(dataObject),
    contentType: 'application/json',
    dataType: 'json',
    success: function(response){
      if(response.status !== 'success'){
        alert("Error processing your request.")
      }
      $("#key"+num).attr('disabled', 'true');
      updateKeyValueHiddenAssign();
    }
  });
}

function refreshServerText(){
  $.ajax({
    type: 'GET',
    url: 'http://'+hostname+'/read',
    success: function(response){
      if(response.status === 'success'){
        $("#serverTextArea").html(response.data.lines.join("\n"));
      }else{
        alert("Error processing your request.")
      }
    }
  });
}

function generateKeyValues(){
  $.ajax({
    type: 'GET',
    url: 'http://'+hostname+'/keys',
    success: function(response){
      if(response.status === 'success'){
        var keys = Object.keys(response.data);
        for(var i=0;i<keys.length;i++){
          createKeyValue(keys[i],response.data[keys[i]],true);
        }
      }else{
        alert("Error processing your request.")
      }
    }
  });
}

function generate(){
  var keys = [];
  $(".keysDropdown").each(function(){
    keys.push($(this).val());
  });
  var dataObject = {
    data: {
      keys
    }
  }
  $.ajax({
    type: 'POST',
    url: 'http://'+hostname+'/write',
    data: JSON.stringify(dataObject),
    contentType: 'application/json',
    dataType: 'json',
    success: function(response){
      if(response.status !== 'success'){
        alert("Error processing your request.")
      }
      refreshServerText();
    }
  });
}

function updateKeyValueHiddenAssign(){
  $.ajax({
    type: 'GET',
    url: 'http://'+hostname+'/keys',
    success: function(response){
      if(response.status === 'success'){
        $(".keysDropdown").each(function(){
          var selected = $(this).val();
          $(this).html('');
          var keys = Object.keys(response.data);
          for(var i=0;i<keys.length;i++){
            var key = keys[i];
            var keyOption = document.createElement("option");
            keyOption.value = key;
            keyOption.innerHTML = key;
            $(this).append(keyOption);
          }
          $(this).val(selected);
        });
      }else{
        alert("Error processing your request.")
      }
    }
  });
}

function updateHiddenValue(id){
  $.ajax({
    type: 'GET',
    url: 'http://'+hostname+'/keys?key='+$("#hka"+id).val(),
    success: function(response){
      if(response.status === 'success'){
        $("#hva"+id).val(response.data);
      }else{
        alert("Error processing your request.")
      }
    }
  });
}

function createHiddenKeyValue(){
  var div = document.createElement("div");
  div.className = "hiddenKeyValueAssign";

  var select = document.createElement("select");
  select.id = "hka"+hiddenKeyValueNum;
  select.className = "keysDropdown";
  select.setAttribute("onchange", "updateHiddenValue("+hiddenKeyValueNum+")");

  var input = document.createElement("input");
  input.id = "hva"+hiddenKeyValueNum;
  input.className = "valuesInput formInput";
  input.setAttribute("readonly", "true");
  input.setAttribute("value", "");

  div.appendChild(select);
  div.appendChild(input);
  $("#keyAssignContainer").append(div);

  updateKeyValueHiddenAssign();
  hiddenKeyValueNum++;
}

function createKeyValue(key, value, disabled){
  var div = document.createElement("div");
  div.id = "kv"+keyValueNum;
  div.className = "keyvalue form-row";

  var keyInput = document.createElement("input");
  var valueInput = document.createElement("input");

  keyInput.id = "key"+keyValueNum;
  valueInput.id = "value"+keyValueNum;

  keyInput.className = "keyInput formInput";
  valueInput.className = "valueInput formInput";

  keyInput.value = key;
  valueInput.value = value;

  if(disabled){
    keyInput.setAttribute("disabled", 'true');
  }

  var updateButton = document.createElement("button");
  updateButton.id = "updateKeyValueButton"+keyValueNum;
  updateButton.innerHTML = "Update";
  updateButton.className = "updateKeyValueButton btn btn-secondary";
  updateButton.setAttribute('onclick', 'updateKeyValue('+keyValueNum+');');

  var deleteButton = document.createElement("button");
  deleteButton.id = "deleteKeyValueButton"+keyValueNum;
  deleteButton.innerHTML = "Delete";
  deleteButton.className = "deleteKeyValueButton btn btn-danger";
  deleteButton.setAttribute('onclick', 'deleteKeyValue('+keyValueNum+');');

  div.appendChild(keyInput);
  div.appendChild(valueInput);
  div.appendChild(updateButton);
  div.appendChild(deleteButton);
  document.getElementById("keyValueContainer").appendChild(div);

  keyValueNum++;
}
