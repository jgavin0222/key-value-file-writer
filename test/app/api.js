var app = require('../../app/index.js');
var chai = require('chai');
var assets = require('../../app/assets.js');
var request = require('supertest');
var assert = require('assert');
var should = require('should');
var fs = require('fs');

var data = {
  key: 'abc',
  value: 'efg'
};

var assertData = {
  [data.key]: data.value
};

fs.writeFileSync(assets.fileJson, JSON.stringify({}));

describe('POST /keys', ()=>{
  it('responds successfully when correct data is used', (done)=>{
    var dataObject = {
      data
    };
    request(app).
      post('/keys').
      send(dataObject).
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(201);
        res.body.status.should.equal('success');
        JSON.stringify(res.body.data).should.equal(JSON.stringify(data));

        return done();
    });
  });
  it('fails when data is not present', (done)=>{
    var dataObject = {};
    request(app).
      post('/keys').
      send(dataObject).
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(400);
        res.body.status.should.equal('error');
        res.body.error.should.equal('missing parameter(s)');

        return done();
    });
  });
});

describe('GET /keys', ()=>{
  it('responds successfully with data', (done)=>{
    request(app).
      get('/keys').
      send().
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);
        res.body.status.should.equal('success');
        (typeof res.body.data).should.equal('object');
        JSON.stringify(res.body.data).should.equal(JSON.stringify(assertData));

        return done();
    });
  });
  it('responds successfully with key specified', (done)=>{
    request(app).
      get('/keys?key='+data.key).
      send().
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);
        res.body.status.should.equal('success');
        (typeof res.body.data).should.equal('string');
        res.body.data.should.equal(data.value);

        return done();
    });
  });
});

describe('GET /read', ()=>{
  it('responds successfully with correct file data', (done)=>{
    fs.writeFileSync(assets.file, data.value+'\n');
    request(app).
      get('/read').
      send().
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);
        res.body.status.should.equal('success');
        (typeof res.body.data).should.equal('object');
        res.body.data.lines[0].should.equal(data.value);

        return done();
    });
  });
});

describe('POST /write', ()=>{
  it('responds successfully when correct data is used', (done)=>{
    var dataObject = {
      data: {
        keys: ['abc']
      }
    };
    request(app).
      post('/write').
      send(dataObject).
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(201);
        res.body.status.should.equal('success');

        return done();
    });
  });
  it('fails when data is not present', (done)=>{
    var dataObject = {};
    request(app).
      post('/write').
      send(dataObject).
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(400);
        res.body.status.should.equal('error');
        res.body.error.should.equal('missing parameter(s)');

        return done();
    });
  });
});

describe('DELETE /keys', ()=>{
  it('responds successfully when correct data is used', (done)=>{
    var {key} = data;
    var dataObject = {
      data: {
        key
      }
    };
    request(app).
      delete('/keys').
      send(dataObject).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);
        res.body.status.should.equal('success');
        res.body.data.key.should.equal(key);

        return done();
    });
  });
  it('fails when data is not present', (done)=>{
    var dataObject = {};
    request(app).
      delete('/keys').
      send(dataObject).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(400);

        return done();
    });
  });
});
