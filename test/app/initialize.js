var app = require('../../app/index.js');
var assets = require('../../app/assets.js');
var chai = require('chai');
var request = require('supertest');
var assert = require('assert');
var should = require('should');


var url = '/version';

describe('GET '+url, ()=>{
  it('responds with correct data', (done)=>{
    request(app).
      get(url).
      send().
      expect('Content-Type', new RegExp('json', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);
        res.body.status.should.equal('success');
        res.body.version.should.equal(process.env.npm_package_version);

        return done();
    });
  });
});

describe('Asset initialization', ()=>{
  it('should return success when file path exists', (done)=>{
    var response = assets.initialize(assets.file, assets.fileJson);

    response.should.equal(true);

    done();
  });
  it('should throw an error when file path does not exist', (done)=>{
    var file = '/tmp/path/does/not/exist/file.txt';
    var errorMessage = 'Path to File does not exist';

    chai.expect(()=>assets.initialize(file, assets.fileJson)).to.throw();

    done();
  });
  it('should throw an error when file json path does not exist', (done)=>{
    var fileJson = '/tmp/path/does/not/exist/file.json';
    var errorMessage = 'Path to File JSON does not exist';

    chai.expect(()=>assets.initialize(assets.file, fileJson)).to.throw();

    done();
  });
});
