FROM node:latest
WORKDIR /usr/src/app
COPY package*.json ./
ENV NODE_ENV production
RUN npm install
RUN mkdir files
COPY . .
EXPOSE 3000
CMD [ "node", "app/index.js" ]
