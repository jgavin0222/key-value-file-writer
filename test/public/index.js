var app = require('../../app/index.js');
var chai = require('chai');
var request = require('supertest');
var assert = require('assert');
var should = require('should');

describe('GET /', ()=>{
  it('should respond successfully with an html page', (done)=>{
    request(app).
      get('/').
      send().
      expect('Content-Type', new RegExp('html', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);

        return done();
    });
  });
});
