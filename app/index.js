const dotEnv = require('dotenv').config();
const bodyParser = require('body-parser');
const express = require('express');
const assets = require('./assets');
const app = express();
const port = process.env.PORT || 3000;

assets.initialize(assets.file, assets.fileJson);
console.log('Server is listening on port '+port);

app.use(bodyParser.json());
app.use(express.static('public'));

app.get('/version', (req, res)=>{
  var response = {
    status: 'success',
    name: 'Node key-value file writer',
    version: process.env.npm_package_version
  };
  res.send(response);
});

app.get('/read', (req, res)=>{
  var response = {
    status: 'success',
    data: {
      lines: []
    }
  };

  response.data.lines = assets.read().split('\n');
  res.status(200).send(response);
});

app.post('/write', (req, res)=>{
  var {body} = req;
  var response = {
    status: 'success'
  };
  var missingParams = {
    status: 'error',
    error: 'missing parameter(s)'
  };

  if(typeof body.data === 'undefined' || typeof body.data.keys === 'undefined'){
    res.status(400).send(missingParams);
  }else{
    response.status = assets.generate(req.body.data.keys);
    res.status(201).send(response);
  }
});

app.get('/keys', (req, res)=>{
  var response = {
    status: 'success',
    data: []
  };
  response.data = assets.readData();
  if(typeof req.query.key !== 'undefined'){
    response.data = response.data[req.query.key];
  }
  res.status(200).send(response);
});

app.post('/keys', (req, res)=>{
  var {data} = req.body;
  var response = {
    data
  };
  var missingParams = {
    status: 'error',
    error: 'missing parameter(s)'
  };
  if(
    typeof data === 'undefined' ||
    typeof data.key === 'undefined' ||
    typeof data.value === 'undefined'){
    res.status(400).send(missingParams);
  }else{
    response.status = assets.writeData(data);
    res.status(201).send(response);
  }
});

app.delete('/keys', (req, res)=>{
  var {data} = req.body;
  var response = {
    data
  };
  var missingParams = {
    status: 'error',
    error: 'missing parameter(s)'
  };
  if(typeof data === 'undefined' || typeof data.key === 'undefined'){
    res.status(400).send(missingParams);
  }else{
    response.status = assets.removeData(data);
    res.status(200).send(response);
  }
});

module.exports = app.listen(port);
