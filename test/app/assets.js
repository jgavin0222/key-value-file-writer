var dotEnv = require('dotenv').config();
var assets = require('../../app/assets.js');
var path = require('path');
var chai = require('chai');
var request = require('supertest');
var assert = require('assert');
var should = require('should');
var fs = require('fs');

var key = 'abc';
var value = 'efg';

describe('Read Data Function', ()=>{
  var dataObject = {
    [key]: value
  };

  it('returns true when file is present', (done)=>{
    fs.writeFileSync(assets.fileJson, JSON.stringify(dataObject));
    assets.readData()[key].should.equal(value);

    done();
  });
  it('returns an empty array when file doesnt exist', (done)=>{
    fs.unlinkSync(assets.fileJson);
    assets.readData().length.should.equal(0);

    done();
  });
});

describe('Generate Plain Text File', ()=>{
  var dataObject = {
    [key]: value
  };
  it('returns true when file.json is present', (done)=>{
    fs.writeFileSync(assets.fileJson, JSON.stringify(dataObject));
    assets.generate([key]).should.equal('success');

    done();
  });
  it('returns failure when file.json is not present', (done)=>{
    fs.unlinkSync(assets.fileJson);
    assets.generate([key]).should.equal('failure');

    done();
  });
  it('deletes text file if it exists', (done)=>{
    fs.writeFileSync(assets.fileJson, JSON.stringify(dataObject));
    fs.writeFileSync(assets.file, 'placeholder');
    assets.generate([key]).should.equal('success');

    done();
  });
});

describe('Read Text File', ()=>{
  it('returns text file if it exists', (done)=>{
    fs.writeFileSync(assets.file, value);
    assets.read().should.equal(value);

    done();
  });
  it('returns an empty array if it does not exist', (done)=>{
    fs.unlinkSync(assets.file);
    assets.read().length.should.equal(0);

    done();
  });
});
