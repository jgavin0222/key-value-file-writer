var app = require('../../app/index.js');
var chai = require('chai');
var request = require('supertest');
var assert = require('assert');
var should = require('should');

describe('GET Javascript /js/', ()=>{
  it('/js/jquery.min.js should respond successfully with a javascript file', (done)=>{
    request(app).
      get('/js/jquery.min.js').
      send().
      expect('Content-Type', new RegExp('javascript', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);

        return done();
    });
  });
  it('/js/keyValueFileWriter.js should respond successfully with a javascript file', (done)=>{
    request(app).
      get('/js/keyValueFileWriter.js').
      send().
      expect('Content-Type', new RegExp('javascript', 'u')).
      end((err, res)=>{
        if(err){
          return done(err);
        }

        res.status.should.equal(200);

        return done();
    });
  });
});
